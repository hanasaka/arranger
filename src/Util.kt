fun MutableList<kotlin.String>.addRegions(entity: TopLevelEntity) {

    val bottomRegion = "    //endregion\n"

    var topRegion = when (entity) {
        TopLevelEntity.INJECT -> "\n    //region INJECTED CLASSES ----------------------------------------------------------------------\n"
        TopLevelEntity.INTERFACE -> "    //region PUBLIC INTERFACES ---------------------------------------------------------------------\n"
        TopLevelEntity.COMPANION -> "    //region COMPANION OBJECT ----------------------------------------------------------------------\n"
        TopLevelEntity.PUBLIC_PROP -> "    //region PUBLIC PROPERTIES ----------------------------------------------------------------------\n"
        TopLevelEntity.PRIVATE_PROP -> "    //region PRIVATE PROPERTIES ----------------------------------------------------------------------\n"
        TopLevelEntity.INIT -> "    //region INITIALIZER -------------------------------------------------------------------------\n"
        TopLevelEntity.OVERRIDE_FUN -> "    //region OVERRIDE FUNCTIONS -------------------------------------------------------------------------\n"
        TopLevelEntity.PUBLIC_FUN -> "    //region PUBLIC FUNCTIONS -------------------------------------------------------------------------\n"
        TopLevelEntity.PRIVATE_FUN -> "    //region PRIVATE FUNCTIONS -----------------------------------------------------------------------\n"
        TopLevelEntity.INNER_CLASSES -> "    //region INNER CLASSES -----------------------------------------------------------------------\n"
        else -> ""
    }
    if (!this.isNullOrEmpty()) {
        this.add(0, topRegion)
        if (entity == TopLevelEntity.INJECT || entity == TopLevelEntity.PUBLIC_PROP ||
            entity == TopLevelEntity.PRIVATE_PROP
        ) {
            this.add(this.size, "")
        }
        this.add(this.size, bottomRegion)
    }
}

fun String?.ifRegionComment(): Boolean {
    this?.let {
        return (it.trim().startsWith("//endregion") || it.trim().startsWith("// endregion")
                || it.trim().startsWith("//region") || it.trim().startsWith("// region"))
    }
    return false
}