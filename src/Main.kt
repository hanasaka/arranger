import java.io.*

enum class TopLevelEntity {
    INJECT,
    INTERFACE,
    COMPANION,
    PUBLIC_PROP,
    PRIVATE_PROP,
    INIT,
    OVERRIDE_FUN,
    PUBLIC_FUN,
    PRIVATE_FUN,
    INNER_CLASSES,
    COMMENT,
    TOP_CLASS,
    NONE
}

fun main() {
    val file = File("src//shit.txt")

    var topClassFlag = false

    val allLines = mutableListOf<String>()
    val overrideFuns = mutableListOf<String>()
    val publicFuns = mutableListOf<String>()
    val privateFuns = mutableListOf<String>()
    val inits = mutableListOf<String>()
    val interfaces = mutableListOf<String>()
    val companions = mutableListOf<String>()
    val innerClasses = mutableListOf<String>()
    val comments = mutableListOf<String>()
    val packages = mutableListOf<String>()
    val imports = mutableListOf<String>()
    var topClass = mutableListOf<String>()
    var publicProps = mutableListOf<String>()
    var privateProps = mutableListOf<String>()

    var injectedClassesList = mutableListOf<String>()

    BufferedReader(FileReader(file)).use { br ->
        val values = mutableListOf<String>()
        var line: String?
        while (br.readLine().also { line = it } != null) {
            line?.let { line ->
                when {
                    line.trim().startsWith("@Inject") -> values.add(line.substringAfter("var "))
                    line.trim().startsWith("package") -> packages.add(line)
                    line.trim().contains("import") -> imports.add(line)
                    else -> {
                    }
                }
            }
        }
        values.sort()
        imports.sort()

        injectedClassesList = values.map { "    @Inject lateinit var $it" }.toMutableList()
        injectedClassesList.addRegions(TopLevelEntity.INJECT)
    }

    BufferedReader(FileReader(file)).use { br ->
        var line: String?
        var startLatch = false
        var latch = TopLevelEntity.NONE
        var braces = 0
        var closes = 0

        while (br.readLine().also { line = it } != null) {
            if (line.ifRegionComment()) continue
            line?.let { line ->
                when {
                    line.trim().startsWith("//") || line.trim().startsWith("/*")
                            || line.trim().startsWith("*") -> {
                        if (latch == TopLevelEntity.NONE) {
                            comments.add(line)
                            latch = TopLevelEntity.COMMENT
                        }
                    }
                    line.trim().startsWith("interface") -> latch = TopLevelEntity.INTERFACE
                    line.trim().startsWith("class") -> {
                        latch = TopLevelEntity.TOP_CLASS
                        topClassFlag = true
                        startLatch = true
                    }
                    line.trim().startsWith("companion object") -> latch = TopLevelEntity.COMPANION
                    line.trim().contains("inner class") || line.trim().contains("internal class") ||
                            (line.trim().contains("class") && topClassFlag) -> {
                        latch = TopLevelEntity.INNER_CLASSES
                        startLatch = true
                    }
                    line.trim().startsWith("init") -> latch = TopLevelEntity.INIT
                    line.trim().startsWith("override fun") -> if (latch == TopLevelEntity.NONE) latch =
                        TopLevelEntity.OVERRIDE_FUN
                    line.trim().startsWith("fun") -> if (latch == TopLevelEntity.NONE) latch = TopLevelEntity.PUBLIC_FUN
                    line.trim().startsWith("private fun") -> if (latch == TopLevelEntity.NONE) latch =
                        TopLevelEntity.PRIVATE_FUN
                    line.trim().startsWith("var") || line.trim().startsWith("val") -> if (latch == TopLevelEntity.NONE) latch =
                        TopLevelEntity.PUBLIC_PROP
                    line.trim().startsWith("private var") || line.trim().startsWith("private val") -> if (latch == TopLevelEntity.NONE) latch =
                        TopLevelEntity.PRIVATE_PROP
                }
                when (latch) {
                    TopLevelEntity.INTERFACE,
                    TopLevelEntity.COMPANION,
                    TopLevelEntity.INNER_CLASSES,
                    TopLevelEntity.INIT,
                    TopLevelEntity.OVERRIDE_FUN,
                    TopLevelEntity.PUBLIC_FUN,
                    TopLevelEntity.PRIVATE_FUN -> {
                        if (startLatch && line.contains("{")) startLatch = false
                        if (line.contains("{") && !line.contains("}")) {
                            braces++
                        } else if (line.contains("}") && !line.contains("{")) {
                            closes++
                        }
                        if (comments.isNotEmpty()) {
                            when (latch) {
                                TopLevelEntity.INTERFACE -> {
                                    interfaces.addAll(comments)
                                    interfaces.add(line)
                                }
                                TopLevelEntity.COMPANION -> {
                                    companions.addAll(comments)
                                    companions.add(line)
                                }
                                TopLevelEntity.INNER_CLASSES -> {
                                    innerClasses.addAll(comments)
                                    innerClasses.add(line)
                                }
                                TopLevelEntity.INIT -> {
                                    inits.addAll(comments)
                                    inits.add(line)
                                }
                                TopLevelEntity.OVERRIDE_FUN -> {
                                    overrideFuns.addAll(comments)
                                    overrideFuns.add(line)
                                }
                                TopLevelEntity.PUBLIC_FUN -> {
                                    publicFuns.addAll(comments)
                                    publicFuns.add(line)
                                }
                                TopLevelEntity.PRIVATE_FUN -> {
                                    privateFuns.addAll(comments)
                                    privateFuns.add(line)
                                }
                            }

                            comments.clear()
                        } else{
                            when (latch) {
                                TopLevelEntity.INTERFACE -> interfaces.add(line)
                                TopLevelEntity.COMPANION -> companions.add(line)
                                TopLevelEntity.INNER_CLASSES -> innerClasses.add(line)
                                TopLevelEntity.INIT -> inits.add(line)
                                TopLevelEntity.OVERRIDE_FUN -> overrideFuns.add(line)
                                TopLevelEntity.PUBLIC_FUN -> publicFuns.add(line)
                                TopLevelEntity.PRIVATE_FUN -> privateFuns.add(line)
                            }
                        }
                        if (closes == braces && !startLatch) {
                            when (latch) {
                                TopLevelEntity.INTERFACE -> interfaces.add("")
                                TopLevelEntity.COMPANION -> companions.add("")
                                TopLevelEntity.INNER_CLASSES -> innerClasses.add("")
                                TopLevelEntity.INIT -> inits.add("")
                                TopLevelEntity.OVERRIDE_FUN -> overrideFuns.add("")
                                TopLevelEntity.PUBLIC_FUN -> publicFuns.add("")
                                TopLevelEntity.PRIVATE_FUN -> privateFuns.add("")
                            }
                            latch = TopLevelEntity.NONE
                        }

                    }
                    TopLevelEntity.PUBLIC_PROP -> {
                        if (comments.isNotEmpty()) {
                            publicProps.addAll(comments)
                            comments.clear()
                        }
                        publicProps.add(line)
                        latch = TopLevelEntity.NONE
                    }
                    TopLevelEntity.PRIVATE_PROP -> {
                        if (comments.isNotEmpty()) {
                            privateProps.addAll(comments)
                            comments.clear()
                        }
                        privateProps.add(line)
                        latch = TopLevelEntity.NONE
                    }
                    TopLevelEntity.TOP_CLASS -> {
                        if (comments.isNotEmpty()) {
                            topClass.addAll(comments)
                            comments.clear()
                        }
                        topClass.add(line)
                        if (startLatch && line.contains("{")) latch = TopLevelEntity.NONE
                    }
                    TopLevelEntity.COMMENT -> latch = TopLevelEntity.NONE
                }
            }
        }
    }

    interfaces.addRegions(TopLevelEntity.INTERFACE)
    companions.addRegions(TopLevelEntity.COMPANION)
    innerClasses.addRegions(TopLevelEntity.INNER_CLASSES)
    inits.addRegions(TopLevelEntity.INIT)
    publicProps.addRegions(TopLevelEntity.PUBLIC_PROP)
    privateProps.addRegions(TopLevelEntity.PRIVATE_PROP)
    overrideFuns.addRegions(TopLevelEntity.OVERRIDE_FUN)
    publicFuns.addRegions(TopLevelEntity.PUBLIC_FUN)
    privateFuns.addRegions(TopLevelEntity.PRIVATE_FUN)

    allLines.addAll(packages)
    allLines.add("")
    allLines.addAll(imports)
    allLines.add("")
    allLines.addAll(topClass)
    allLines.add("")
    allLines.addAll(injectedClassesList)
    allLines.addAll(interfaces)
    allLines.addAll(companions)
    allLines.addAll(publicProps)
    allLines.addAll(privateProps)
    allLines.addAll(inits)
    allLines.addAll(overrideFuns)
    allLines.addAll(publicFuns)
    allLines.addAll(privateFuns)
    allLines.addAll(innerClasses)
    allLines.add("}")

    for (line in allLines) {
        println(line)
    }

    PrintWriter(FileWriter("src//res.txt")).use {
        for (line in allLines) {
            it.println(line)
        }
    }
}